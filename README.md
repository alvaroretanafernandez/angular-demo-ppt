# YOUR FIRST ANGULAR APP #

Demo Class PowerPoint

### Featuring ###

* Angular CLI
* Angular Bootstrap
* Angular Modules
* Angular HttpService
* Angular Data Resolver

### What you need ###

* Laptop
* node + 6.x

### aretanafernandez@gmail.com ###

* Full example at https://bitbucket.org/alvaroretanafernandez/angular-demo
